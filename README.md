# pic18f47q10-flash-demo

#### 介绍
基于MCC配置的PIC18F47Q10 Flash读写示例

#### 硬件说明

- 硬件采用了 **PIC18F47Q10 CURIOSITY NANO** 
- 官网地址：[https://www.microchip.com/en-us/development-tool/DM182029](https://www.microchip.com/en-us/development-tool/DM182029)

#### 串口配置:
| 资源     | 说明     |
|--------|--------|
| MCU TX | RD0    |
| MCU RX | RD1    |
| 波特率    | 115200 |

#### 软件说明

1.  底层代码完全采用MCC Melody配置而成
2.  编写了简单的应用示例
3.  可以通过该EVB自带的虚拟串口进行交互
4.  交互内容请参考 demo-capture.png
5.  **!!注意: 在项目属性的编译器配置中，在ROM Range使用了“-10000-1ffff”参数，使得编译器预留出这部分Flash空间供Flash擦写实验**

