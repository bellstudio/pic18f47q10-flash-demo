 /*
 * MAIN Generated Driver File
 * 
 * @file main.c
 * 
 * @defgroup main MAIN
 * 
 * @brief This is the generated driver implementation file for the MAIN driver.
 *
 * @version MAIN Driver Version 1.0.0
*/

/*
? [2023] Microchip Technology Inc. and its subsidiaries.

    Subject to your compliance with these terms, you may use Microchip 
    software and any derivatives exclusively with Microchip products. 
    You are responsible for complying with 3rd party license terms  
    applicable to your use of 3rd party software (including open source  
    software) that may accompany Microchip software. SOFTWARE IS ?AS IS.? 
    NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS 
    SOFTWARE, INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT,  
    MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT 
    WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY 
    KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF 
    MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE 
    FORESEEABLE. TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP?S 
    TOTAL LIABILITY ON ALL CLAIMS RELATED TO THE SOFTWARE WILL NOT 
    EXCEED AMOUNT OF FEES, IF ANY, YOU PAID DIRECTLY TO MICROCHIP FOR 
    THIS SOFTWARE.
*/
#include "mcc_generated_files/system/system.h"
#include "stdio.h"

/*
    Main application
*/
#define PFM_ADDR    0x10000UL
#define PFM_SECTOR_SIZE 128     //in WORDS


uint16_t wr_arry[PFM_SECTOR_SIZE] = {0,0,0,0,0,0,0,0,
                                    1,2,3,4,5,6,7,8,
                                    9,10,11,12,13,14,15,16};

uint16_t wr_arry2[PFM_SECTOR_SIZE] = {1,2,3,4,5,6,7,8,
                                    9,10,11,12,13,14,15,16,
                                    0xaa, 0x55, 0xff, 0x00, 0xa5, 0x5a, 0xf0, 0x0f};

uint16_t rd_arry[PFM_SECTOR_SIZE] = {};


int main(void)
{
    SYSTEM_Initialize();

    // If using interrupts in PIC18 High/Low Priority Mode you need to enable the Global High and Low Interrupts 
    // If using interrupts in PIC Mid-Range Compatibility Mode you need to enable the Global and Peripheral Interrupts 
    // Use the following macros to: 

    // Enable the Global Interrupts 
    INTERRUPT_GlobalInterruptEnable(); 

    // Disable the Global Interrupts 
    //INTERRUPT_GlobalInterruptDisable(); 

    // Enable the Peripheral Interrupts 
    INTERRUPT_PeripheralInterruptEnable(); 

    // Disable the Peripheral Interrupts 
    //INTERRUPT_PeripheralInterruptDisable(); 

    printf("Ready for command:\r\n");
    printf("**Erase before re-write**\r\n");
    printf("w - Write array at 0x10000\r\n");
    printf("x - Write array2 at 0x10000\r\n");
    printf("e - Erase sector at 0x10000\r\n");
    printf("r - Read array at 0x10000\r\n");

    while(1)
    {
        if (EUSART2_IsRxReady())
        {
            switch (EUSART2_Read())
            {
                case 'w':
                    NVM_UnlockKeySet(UNLOCK_KEY_ROW_WRITE);
                    if(NVM_OK == FLASH_RowWrite(PFM_ADDR, (flash_data_t *)wr_arry))
                    {
                        printf("Row Write1 OK!\r\n");
                    }
                    else
                    {
                        printf("Row Write1 ERR!\r\n");
                    }                    
                    NVM_UnlockKeyClear();
                    break;
                case 'x':
                    NVM_UnlockKeySet(UNLOCK_KEY_ROW_WRITE);
                    if(NVM_OK == FLASH_RowWrite(PFM_ADDR, (flash_data_t *)wr_arry2))
                    {
                        printf("Row Write2 OK!\r\n");
                    }
                    else
                    {
                        printf("Row Write2 ERR!\r\n");
                    }                    
                    NVM_UnlockKeyClear();
                    break;
                case 'e':
                    NVM_UnlockKeySet(UNLOCK_KEY_PAGE_ERASE);
                    if (NVM_OK == FLASH_PageErase(PFM_ADDR))
                    {
                        printf("Page Erased!\r\n");
                    }
                    else
                    {
                        printf("Page Erase FAIL!\r\n");
                    }
                    NVM_UnlockKeyClear();
                    break;
                case 'r':
                    NVM_UnlockKeySet(UNLOCK_KEY_ROW_READ);
                    if (NVM_OK == FLASH_RowRead(PFM_ADDR, (flash_data_t *)rd_arry))
                    {
                        printf("Page Read OK! Display first 32 WORDS\r\n");
                        for (uint8_t loop = 0; loop < 8; loop++)
                        {
                            printf("%4X ", rd_arry[loop]);
                        }
                            printf("\r\n");
                        for (uint8_t loop = 0; loop < 8; loop++)
                        {
                            printf("%4X ", rd_arry[loop+8]);
                        }
                            printf("\r\n");
                        for (uint8_t loop = 0; loop < 8; loop++)
                        {
                            printf("%4X ", rd_arry[loop+16]);
                        }
                            printf("\r\n");
                        for (uint8_t loop = 0; loop < 8; loop++)
                        {
                            printf("%4X ", rd_arry[loop+24]);
                        }
                            printf("\r\n");
                    }
                    else
                    {
                        printf("Page Read FAIL!\r\n");
                    }
                    NVM_UnlockKeyClear();
                    break;
                default:
                    printf("BAD command!\r\n");
                    break;
            }
        }
    }    
}